(ns devbot.util
  (:require [instaparse.core :as insta])
  (:require [rhizome.viz :as viz]))

(defn in?
  "True if item `item` is in collection `coll`."
  ;; Source: https://stackoverflow.com/a/3249777
  [item coll]
  (some #(= item %) coll))

(defn in-let
  "Similar to in?, but when true, returns the value that yielded true."
  [item coll]
  (if (some #(= item %) coll)
    (filter #(= item %) coll)
    false))

(defn input
  "command line user input, where `prompt` is a string prompt. If no string is supplied, a standard '=> ' is used."
  [prompt]
  (do
    (print prompt)
    (flush)
    (read-line))
  []
  (do
    (print "=> ")
    (flush)
    (read-line)))

(defn doc-string
  ""
  ;; Source: https://stackoverflow.com/a/43451051
  [symbol]
  (:doc (meta (resolve symbol))))


(defn tree-map
  ""
  [f coll]
  (map
    (fn mapfn [elem]
      (if (seq? elem)
        (map mapfn elem)
        (f elem)))
    coll))