(ns devbot.bot
  (:require [devbot.util :refer :all]
            [devbot.link-parser :as link]
            [clojure.core.match :refer [match]]
            [clojure.string :as str]
            [instaparse.core :as insta]
            [rhizome.viz :as viz]
            [clojure.java.shell :as sh]
            [java-time :as jtime]
            [clojure.java.data :as jdata]
            [clojure.java.io :as io]
            [clojure.edn :as edn])
  (:import [net.dv8tion.jda.core AccountType JDABuilder JDA EmbedBuilder Permission]
           [net.dv8tion.jda.core.hooks ListenerAdapter EventListener]
           [net.dv8tion.jda.core.entities Guild ChannelType RichPresence Game Webhook MessageEmbed MessageChannel Message User]
           [net.dv8tion.jda.core.events.message MessageReceivedEvent]
           (java.io StringWriter PrintWriter File)
           (net.dv8tion.jda.core.events ReadyEvent)
           (net.dv8tion.jda.core.events.message.react GenericMessageReactionEvent MessageReactionAddEvent)
           (net.dv8tion.jda.webhook WebhookMessageBuilder WebhookClientBuilder))
  (:gen-class))

;; TODO: Create an extensible embed menu system, with reaction inputs
;; EMOJIS FOR INPUT MENUS
;; :zero:
;; :one:
;; :two:
;; :three:
;; :four:
;; :five:
;; :six:
;; :seven:
;; :eight:
;; :nine:
;; :arrow_backward:
;; :arrow_forward:
;; :arrow_left:
;; :arrow_right:
;; :white_check_mark:
;; :negative_squared_cross_mark:
;; :arrow_right_hook:
;; :arrows_counterclockwise:
;; :leftwards_arrow_with_hook:
;; :information_source:
;; :hash:
;; :asterisk:

(defn- event-listener
  "Takes in a JDA object `jda`, and a function to handle messages `message-handler`, and constructs a
  proxy object derived from ListenerAdapter that calls `message-handler` in overriding `onMessageReceived`."
  ([event-handler]
   (proxy [EventListener] []
     (onEvent [event] (event-handler event)))))

(defn- add-fields-to-embed
  "Helper function for `bot/embed`. Handles adding fields to the embed safely, regardless of the number of fields in the embed.
  For each field, the `:inline?` key can safely be left out, and is assumed to yield a value of `false` if it is."
  [embed fields]
  ;; TODO: maybe make this a bit prettier later
  (doto embed
    (#(doseq [f fields]
        (.addField % (:name f) (:value f) (boolean (:inline? f)))))))

(defn- add-author-to-embed
  "Helper function for `bot/embed`. Adds author information safely, implementing all 3 overloads (name url icon-url,
  name url, and just name)."
  [embed map]
  (cond
     (and (:name map) (:url map) (:icon-url map)) (.setAuthor embed (:name map) (:url map) (:icon-url map))
     (and (:name map) (:url map)) (.setAuthor embed (:name map) (:url map))
     (and (:name map) (:icon-url map)) (.setAuthor embed (:name map) nil (:icon-url map))
     (and (:name map)) (.setAuthor embed (:name map))))

(defn- add-footer-to-embed
  "Helper function for `bot/embed`. Implements all 2 overloads (text icon-url, and just icon-url)"
  [embed map]
  (cond
    (and (:icon-url map) (:text map)) (.setFooter embed (:text map) (:icon-url map))
    ;; (:text map) (.setFooter embed (:text map) nil) TODO: find some workaround for this
    (:icon-url map) (.setFooter embed nil (:icon-url map))))


(defn embed-menu
  ""
  ;; TODO: actually do
  [embed-map]
  (.build (cond-> (new EmbedBuilder)
            (:title embed-map) (.setTitle (:title embed-map))
            (:description embed-map) (.setDescription (:description embed-map))
            (:author embed-map) (add-author-to-embed (:author embed-map))
            (:timestamp embed-map) (.setTimestamp (:timestamp embed-map))
            (:color embed-map) (.setColor (:color embed-map))
            (:footer embed-map) (add-footer-to-embed (:footer embed-map))
            (:fields embed-map) (add-fields-to-embed (:fields embed-map))
            (:thumbnail embed-map) (.setThumbnail (:thumbnail embed-map)))))

(defn embed
  "from a map, returns an embed, ready to be sent!"
  ;; TODO: Test thumbnail
  [embed-map]
  (cond-> (new EmbedBuilder)
     (:title embed-map) (.setTitle (:title embed-map))
     (:description embed-map) (.setDescription (:description embed-map))
     (:author embed-map) (add-author-to-embed (:author embed-map))
     (:timestamp embed-map) (.setTimestamp (jtime/local-date-time (:timestamp embed-map)))
     (:image embed-map) (.setImage (:image embed-map))
     (:color embed-map) (.setColor (:color embed-map))
     (:footer embed-map) (add-footer-to-embed (:footer embed-map))
     (:fields embed-map) (add-fields-to-embed (:fields embed-map))
     (:thumbnail embed-map) (.setThumbnail (:thumbnail embed-map))
    :finally (.build)))

(comment
  (defn embeds
    ;; TODO: implement better and test
    "from a list of maps representing embeds, returns a message ready to be sent!"
    [embed-list]
    (let [webhook-client (-> (Webhook/newClient) (.build))
          embeds (map embeds embed-list)]
      (-> webhook-client (-> (new WebhookMessageBuilder) (.addEmbeds embeds) (.build))))))

(defn delete-message
  "Deletes message `msg`."
  [msg]
  (.. msg (delete) (queue)))

(defn event->msg
  "Returns an event's message as a string"
  [event]
  (.. event (getMessage) (getContentDisplay)))

(defn send-message
  "Sends a message `msg` to `location`.
  `location` can be a User, Channel, etc.
  `msg` can be a string representing message content, Message, File,or a map. If it is a map, `msg`
    requires an `:as` key to be present in order to allow for proper casting. Accepted values for `:as` are
    `:message`, `:embed`, `:embeds`, and `:webhook-message`."
  [location msg]
  (let [channel (if (instance? User location) (-> location (.openPrivateChannel) (.complete)) location)
        send-msg #(-> channel (.sendMessage %) (.queue))]
    (cond
      ;; msg is a clojure-style data struct and must be converted to type Message first
      (map? msg)
      (send-msg
        (case (:as msg)
          :message (jdata/to-java Message msg)
          :embed (embed msg)
          :embeds (do nil) ;; TODO: webhook message style multi embed sending
          :webhook-message (do nil))) ;; TODO: implement webhook msgs

      ;; msg is a file
      (instance? File msg) (-> channel (.sendFile msg) (.queue))

      ;; msg is handle-able by JDA
      (or (string? msg) (instance? MessageEmbed msg)) (send-msg msg))))

(defn get-message
  "Gets a message from a location by its message-id."
  [location message-id]
  ;; TODO: TEST
  (let [channel (if (instance? User location) (-> location (.openPrivateChannel) (.complete)) location)]
    (.getMessageById channel)))

(defn edit-message
  "Edits a message given a message object."
  [msg new-msg-content]
  (-> msg (.editMessage new-msg-content)))

(defn send-file
  "Sends the file stored in `path` to channel `channel`"
  [channel path]
  (.. channel (sendFile (clojure.java.io/file path)) (queue)))

(defn send-private-message
  "Sends a private message `msg` to user `user`"
  [user msg]
  (send-message (.. user (openPrivateChannel) (complete)) msg))

;; Embed to test with in discord
(comment
  (def test-embed
    {:title       "title ~~(did you know you can have markdown here too?)~~",
     :description "this supports [named links](https://discordapp.com) on top of the previously shown subset of markdown."
     :url         "https://discordapp.com",
     :color       15520740,
     :footer      {:icon-url "https://cdn.discordapp.com/embed/avatars/0.png",
                   :text     "footer text"}
     :author      {:name     "author name",
                   :url      "https://discordapp.com",
                   :icon-url "https://cdn.discordapp.com/embed/avatars/0.png"}
     :fields      [{:name    "\uD83E\uDD14"
                    :value   "some of these properties have certain limits..."
                    :inline? false}
                   {:name    "\uD83D\uDE31"
                    :value   "try exceeding some of them!"
                    :inline? false}
                   {:name    "\uD83D\uDE44"
                    :value   "an informative error should show up, and this view will remain as-is until all issues are fixed"
                    :inline? false}
                   {:name    "<:NSpider:509920710727172116>"
                    :value   "these last two"
                    :inline? true}
                   {:name    "<:mrfractal:504026506154999809>"
                    :value   "are inline"
                    :inline? true}]}))

(defn bot
  "Creates a bot account"
  ;; TODO: Implementation and Documentation
  ;; TODO: Find some way to integrate config files here, or in a separate function.
  ;; TODO: Catch javax.security.auth.login.LoginException for if the token is a user token, or otherwise invalid.
  ;; TODO: Find a way to not have to use object-array here, it looks weird af.
  [& {:keys [token event-handler presence]
      ;;:or   {:token         (:token (edn/read-string (slurp (io/resource "config.edn"))))
      ;;       :event-handler #(when (instance? % ReadyEvent) (println "Bot Ready!"))
      ;;       :presence      (:presence (edn/read-string (slurp (io/resource "config.edn"))))}
      :as   all-specified}]
  (cond-> (new JDABuilder token)
    (some? event-handler) (-> (.addEventListener (object-array [(event-listener #(event-handler %))])))
    (some? presence) (-> (.setGame (Game/playing presence)))
    :finally (.build)))