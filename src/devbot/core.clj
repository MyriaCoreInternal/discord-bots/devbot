(ns devbot.core
  (:require [devbot.bot :as bot]
            ;;[devbot.gdocs]
            [devbot.util :refer :all]
            [devbot.link-parser :as link]
            [clojure.core.match :refer [match]]
            [clojure.string :as str]
            [instaparse.core :as insta]
            [rhizome.viz :as viz]
            [clojure.java.shell :as sh]
            [clojure.edn :as edn]
            [java-time]
            [clojure.pprint :refer [pprint]])
  (:import [net.dv8tion.jda.core.entities Guild ChannelType PrivateChannel]
           [net.dv8tion.jda.core.events ReadyEvent StatusChangeEvent]
           [net.dv8tion.jda.core.events.http HttpRequestEvent]
           [net.dv8tion.jda.core.events.message MessageReceivedEvent MessageEmbedEvent MessageDeleteEvent]
           [net.dv8tion.jda.core.exceptions InsufficientPermissionException]
           [net.dv8tion.jda.core.events.user.update UserUpdateGameEvent UserUpdateOnlineStatusEvent]
           [net.dv8tion.jda.core.events.user UserTypingEvent]
           [net.dv8tion.jda.core.events.message.guild GuildMessageReceivedEvent GuildMessageEmbedEvent GuildMessageDeleteEvent]
           [net.dv8tion.jda.core.events.message.priv PrivateMessageReceivedEvent PrivateMessageEmbedEvent]
           [net.dv8tion.jda.core.events.channel.priv PrivateChannelCreateEvent]
           [net.dv8tion.jda.core.events.guild GuildReadyEvent]
           [net.dv8tion.jda.core.events.message.react MessageReactionAddEvent MessageReactionRemoveEvent]
           [net.dv8tion.jda.core.events.message.guild.react GuildMessageReactionAddEvent GuildMessageReactionRemoveEvent]
           (net.dv8tion.jda.core Permission))
  (:gen-class))

;; TODO: Add a remind-me command that sends text / pics to the user in a specified timeframe

(def state (atom {:cmd-prefix            #"(?s)^(?:d(?:ev)?)(?:>\s*|\)\s*|\.\s*)(.*)"
                  :accepted-cmd-prefixes ["d." "d>" "d)" "dev." "dev>" "dev)"]}))

(defmulti handle-command
  (fn [^MessageReceivedEvent event]
    (let [msg (.. event (getMessage) (getContentDisplay))]  ;; TODO: make sure this regex isnt retarded
      (-> (re-matches (:cmd-prefix @state) msg)
        (second)
        (str/lower-case)
        (str/split #"\s")
        (first)))))


(defmulti handle-command-help
  (fn [command-args]
    (first command-args)))

(defn bot-controller?
  "Checks to see if the author of an event has bot-controller rights"
  [event]
  (let [author (.getAuthor event)
        member (.getMember event)
        guild-id  (keyword (-> member (.getGuild) (.getId)))]
    (or (-> member (.hasPermission [Permission/ADMINISTRATOR]))
      (.isOwner member)
      (= (-> author (.getId)) "160786816721485825")   ;; my user id
      (some (set (get-in @state [:guild-settings :guilds guild-id :bot-controller-role-ids]))
        (mapv #(.getId %) (-> member (.getRoles)))))))

;; START BOT COMMANDS

(defmethod handle-command "ping"
  ;; Ping Pong!
  [^MessageReceivedEvent event]
  (bot/send-message (.getChannel event) "Pong!"))

(defmethod handle-command-help "ping"
  [args]
  {:cmd-name          "ping"
   :display-name      "Ping"
   :short-description "Ping Pong!"
   :long-description  (str "Pong! Pretty much useless unless you need "
                        "to test bots :yum:")
   :usage             (str (first (:accepted-cmd-prefixes @state)) "ping")})

(defmethod handle-command "parse-tree"
  ;;"Displays a parse tree from a supplied grammar and sentence. Usage: <prefix> parse-tree <sentence inline code> <language grammar in code block>"
  [^MessageReceivedEvent event]
  (let [msg      (.. event (getMessage) (getContentDisplay))
        sentence (last (re-find #"\s+`(.*)`\s+" msg))
        parser   (insta/parser (last (re-find #"(?s)```(.*)```" msg)))]
    (insta/visualize (parser sentence) :output-file (str "resources/images/parse-tree.png"))
    (bot/send-file (.getChannel event) "resources/images/parse-tree.png")))

(defmethod handle-command-help "parse-tree"
  [args]
  {:cmd-name          "parse-tree"
   :display-name      "Parse Tree"
   :short-description "Creates a visual parse tree for a supplied sentence and grammar."
   :long-description  (str "Displays the parse tree for a given sentence in a "
                        "given grammar. Uses the [insta/visualize](https://github.com/Engelberg/instaparse#visualizing-the-tree) "
                        "function from instaparse to achieve this.")
   :usage             (str (first (:accepted-cmd-prefixes @state)) "parse-tree `<insert your sentence here>`" \newline
                        "```" \newline "<insert your grammar here>" \newline "```")})


(defmethod handle-command "dot"
  ;; "Visualizes a dot file into a graph. Usage: <prefix> viz-dot <dot file in code block>"
  [^MessageReceivedEvent event]
  (let [dot (last (re-find #"(?s)```(.*)```" (.. event (getMessage) (getContentDisplay))))]
    (spit "resources/text/dot.gv" dot)
    (sh/sh "dot" "-Tpng" "resources/text/dot.gv" "-o" "resources/images/dot.png")
    (bot/send-file (.getChannel event) "resources/images/dot.png")))

(defmethod handle-command-help "dot"
  [args]
  {:cmd-name          "dot"
   :display-name      "View Dot File"
   :short-description "Creates a picture for a given graphviz dot file."
   :long-description  "Renders a picture from the given graphviz dot file."
   :usage             (str (first (:accepted-cmd-prefixes @state)) "dot" \newline
                        "```" \newline
                        "<insert the text from your dot file here>" \newline
                        "```")})

(defmethod handle-command "syntax-tree"
  [^MessageReceivedEvent event]
  ;; "Visualizes an english syntax tree of the sentence given. Usage: <prefix> syntax-tree <sentence in inline code block>"
  (let [sentence (last (re-find #"`(.*)`" (.. event (getMessage) (getContentDisplay))))]
    (rhizome.viz/save-tree next rest
      (link/draw-constituent-tree (link/constituent-tree sentence))
      :directed? false
      :node->descriptor (fn [n] {:label (first n)})
      :filename "resources/images/syntax-tree.png")
    (bot/send-file (.getChannel event) "resources/images/syntax-tree.png")))

(defmethod handle-command-help "syntax-tree"
  [args]
  {:cmd-name          "syntax-tree"
   :display-name      "Display Syntax Tree"
   :short-description "Displays the syntax tree for a given english sentence."
   :long-description  (str "Draws an english parse tree from a given sentence.")
   :usage             (str (first (:accepted-cmd-prefixes @state)) "syntax-tree `<insert your sentence here>`")})


(defmethod handle-command "embed"
  [^MessageReceivedEvent event]
  (let [embed-map (edn/read-string (last (re-find #"(?s)```(.*)```" (.. event (getMessage) (getContentDisplay)))))]
    (bot/send-message (.getChannel event) (bot/embed embed-map))))

(defmethod handle-command-help "embed"
  [args]
  {:cmd-name          "embed"
   :display-name      "Embed"
   :short-description "Displays an embed from a given [edn file](https://github.com/edn-format/edn)."
   :long-description  (str "Creates an embed from a given [edn file](https://github.com/edn-format/edn)." \newline
                        "Accepted keys:" \newline
                        " ```clojure" \newline
                        (with-out-str
                          (clojure.pprint/pprint {:title       'string,
                                                  :description 'string,
                                                  :url         'string
                                                  :color       'int
                                                  :footer      {:icon-url 'string, :footer-text 'string}
                                                  :author      {:name 'string, :icon-url 'string, :url 'string}
                                                  :fields      [{:name 'string, :value 'string, :url 'string} '& 'rest]}))
                        "```")
   :usage             (str (first (:accepted-cmd-prefixes @state)) "embed" \newline
                        "```" \newline
                        "<insert edn embed spec here>" \newline
                        "```")})

(defmethod handle-command "ping-bomb"
  [^MessageReceivedEvent event]
  ;; TODO: Debug
  (let [fuse (edn/read-string (re-find #"[0-9]+" (-> event (.getMessage) (.getContentDisplay))))
        channel (.getChannel event)
        bomb-msg-id-delay (delay (-> channel (.getLatestMessageId)))
        bomb-msg (atom (str ":bomb:" (str/join (repeat fuse "-")) ":fire:"))]
    (bot/send-message channel @bomb-msg)
    (force bomb-msg-id-delay)
    (println @bomb-msg-id-delay)
    (future
      (Thread/sleep 1000)
      (dotimes [n (range fuse)]
        (swap! bomb-msg #(str/replace-first % \- ""))
        (bot/edit-message @bomb-msg-id-delay @bomb-msg)
        (Thread/sleep 1000)))))

(defmethod handle-command-help "ping-bomb"
  [args]
  (if (not (empty? (rest args)))
    (do nil)
    {:cmd-name          "ping-bomb"
     :display-name      "Ping Bomb"
     :short-description "Set a nasty trap!"
     :long-description  (str "Creates a bomb with a fuse, and shortens the fuse every second. " \newline
                          "After a specified amount of time passes, the bomb explodes, and pings everyone!")
     :usage             (str (first (:accepted-cmd-prefixes @state)) "ping-bomb <number of seconds for fuse>")
     :examples          [(str (first (:accepted-cmd-prefixes @state)) "ping-bomb 10")
                         (str (first (:accepted-cmd-prefixes @state)) "ping-bomb 5")]}))


(defn- eval-remind-me
  "Helper function used in the command `remind-me`. Transforms the parse tree into a vector where the
  first element of the vector is the milisecond wait time and the second element is the reminder text"
  [node]
  ;; TODO: Support evaluation of dates
  (if (vector? node)
    (case (first node)
      :units
      (cond
        (re-find #"(?i)seconds?" (last node)) 1000
        (re-find #"(?i)minutes?" (last node)) (* 1000 60)
        (re-find #"(?i)hours?" (last node)) (* 1000 60 60)
        (re-find #"(?i)days?" (last node)) (* 1000 60 60 24)
        (re-find #"(?i)weeks?" (last node)) (* 1000 60 60 24 7)
        (re-find #"(?i)months?" (last node)) (* 1000 60 60 24 30.4167))

      :amount-of-time
      (->> (rest node)
        (apply
          (fn ([amt unit] (* (edn/read-string amt) unit))
            ([amt unit rst] (+ (* (edn/read-string amt) unit rst))))))

      :subject
      (last node)

      :S
      (rest node))
    node))

(defmethod handle-command "remind-me"
  [^MessageReceivedEvent event]
  (let [sentence    (str/replace (bot/event->msg event) #"(?:d(?:ev)*)(?:>\s*|\)\s*|\.\s*)remind-me\s+" "")
        parser      (insta/parser (clojure.java.io/resource "text/reminder-grammar.bnf"))
        eval-result (clojure.walk/postwalk eval-remind-me (parser sentence))]
    (future (Thread/sleep (first eval-result))
      (bot/send-message (.getChannel event) (str (-> event (.getAuthor) (.getAsMention)) " " (second eval-result))))))


(defmethod handle-command-help "remind-me"
  [args]
  {:cmd-name          "remind-me"
   :display-name      "Remind Me"
   :short-description "Remind yourself of something!"
   :long-description  (str
                        "Sends a private message on a certain day, or in a certain amount of time." \newline
                        "*WARNING:* Date functionality is currently down, only use time offsets (i,e, `remind-me in ...`)")
   :usage             (str (first (:accepted-cmd-prefixes @state)) "remind-me [time / date] [reminder content]")
   :examples          [(str (first (:accepted-cmd-prefixes @state)) "remind-me in 4 minutes to do your homework")
                       (str (first (:accepted-cmd-prefixes @state)) "remind-me on 2/6/2019 to get a present for Mom.")]})

(defmethod handle-command "pilot"
  ;; TODO: Add DM navigation based on servers the user and bot share, and the bot's access to channels
  ;; TODO: Add a command to boot the current pilot out, or some pilot management system
  ;; TODO: Make the incoming msg feed better, like put all the emebds in the same one, and allow better image / file support
  [^MessageReceivedEvent event]
  (let [author        (.getAuthor event)
        dm-author     #(bot/send-private-message author %)
        author-guilds (.getMutualGuilds author)]

    ;; Author can use this command when:
    ;; - Author is a server admin
    ;; - Author is me
    ;; - Author has a designated bot controller role for their server
    (when (bot-controller? event)
      (bot/delete-message (.getMessage event))
      (cond
        ;; Nobody's piloting, so author gets to pilot
        (not (:pilot @state))
        (do
          (dm-author "**Entered Pilot Mode!**")
          (swap! state assoc :pilot {:user author, :channel (.getChannel event)}))

        ;; Person asks to pilot while there's another user still piloting.
        (not= (-> @state :pilot :user) author)
        (do
          (dm-author (str "**Cannot enter Pilot Mode: User " (-> @state :pilot :user (.getName))
                       " is currently piloting.**")))

        ;; Pilot asks to get out
        (= (-> @state :pilot :user) author)
        (do
          (dm-author "**Exited Pilot Mode!**")
          (swap! state dissoc :pilot))

        :else (throw (new Exception "Something really bad must have happened for the logic to get here."))))
    (when (not (bot-controller? event))
      (bot/send-message (.getChannel event) (str (.getAsMention author) " **Hey!** You don't have perms for that!")))))


(defmethod handle-command-help "pilot"
  [args]
  {:cmd-name          "pilot"
   :display-name      "Pilot me!"
   :short-description "Speak through the bot in your DM's!"
   :long-description  (str "In order to start speaking through the bot, write d.pilot in the channel of your choice." \newline
                        "All messages that hit that channel afterwards will be sent to your DM's by the bot, and when you "
                        "DM the bot, it will send the same message back into the channel you're 'tracking'.")
   :usage             (str (first (:accepted-cmd-prefixes @state)) "pilot")})

(defmethod handle-command "config"
  [^MessageReceivedEvent event]
  (let [msg (str/split (-> event (.getMessage) (.getContentDisplay)) #"\s+")]
    (when (bot-controller? event)
      (case (second msg)
        ;;;; case if arg is "google-auth"
        ;;"google-auth"
        ;;(bot/send-message (.getChannel event)
        ;;  (str "Here's a google authentication link! Click it!" \newline "http://tinyurl.com/y67vmn5j"))
        ;; case if confused
        (bot/send-message (.getChannel event)
          (str "Sorry, I don't know what you mean by that. Type `"
            (-> @state :accepted-cmd-prefixes (first)) "help config` for details."))))
    (when (not (bot-controller? event))
      (bot/send-message (.getChannel event) " **Hey!** You don't have perms for that!"))))

(defmethod handle-command-help "config"
  [args]
  (cond
    ;;;; for `help config google-auth`
    ;;(in? "google-auth" args)
    ;;{:cmd-name "config google-auth"
    ;; :display-name "Configure Google Authentication"
    ;; :short-description "Post google authentication link!"
    ;; :long-description "Makes bot post Google Authentication link."}

    ;; for `help config`
    :default
    {:cmd-name "config"
     :display-name "Configure Bot"
     :short-description "Configure your bot settings!"
     :long-description (str "**Accepted Sub-commands:**" \newline
                         "*config google-auth*: Sends a google authentication link for use with `"
                         (-> @state :accepted-cmd-prefixes (first)) "say`.")}))

(defmethod handle-command "help"
  [^MessageReceivedEvent event]
  (let [args (nthrest (str/split (-> event (.getMessage) (.getContentDisplay)) #"\s+") 1)]
    (if (not (empty? (first args)))
      (bot/send-message (.getChannel event)
        (bot/embed
          (let [cmd-help-props (handle-command-help args)
                embed          {:title       (str ":grey_question: DevBot Help - " (:display-name cmd-help-props))
                                :color       1922170
                                :description (:long-description cmd-help-props)}]
            (cond-> embed
              ;; has usage notes
              (:usage cmd-help-props)
              (assoc :fields [{:name "Usage" :value (:usage cmd-help-props) :inline? false}])

              ;; has examples
              (:examples cmd-help-props)
              (as-> em
                (assoc em :fields
                          (conj (:fields em) {:name "Examples" :value (str/join (str \newline \newline) (:examples cmd-help-props)) :inline? false})))))))
      (bot/send-message (.getChannel event)
        (bot/embed
          {:title       ":grey_question: DevBot Help"
           :color       1922170
           :description (str "List of commands, what they do, and how to use them." \newline
                          "__*Accepted Command Prefixes:*__  "
                          (str/join ", " (mapv #(str "*`" % "`*") (:accepted-cmd-prefixes @state))))
           :fields      (vec (for [cmd-name (keys (methods handle-command-help))
                                   :let [cmd-help-str (:short-description (handle-command-help [cmd-name]))
                                         display-name (:display-name (handle-command-help [cmd-name]))]]
                               {:name  (str display-name " - `" (first (:accepted-cmd-prefixes @state)) cmd-name "`")
                                :value cmd-help-str}))})))))

(defmethod handle-command-help "help"
  [args]
  {:cmd-name          "help"
   :display-name      "Display Help Dialogue"
   :short-description "Displays information about all commands, or about a specific command."
   :long-description  (str "Displays information about all commands, or about a specific command. " \newline
                        "Help dialogues for specific commands may include usage notes. Listed below is a "
                        "list of syntax, and what they mean in the context of a usage note. " \newline \newline
                        "__*Usage Syntax*__" \newline
                        "\u200B   § < argument description > - anything between two angle brackets (`<` and `>`)" \newline
                        "\u200B     is describing information that the command needs" \newline
                        "\u200B   § <? optional argument description> - if the first diamond tag has a question mark" \newline
                        "\u200B     in front of it, then it's not required for the command." \newline
                        "\u200B   § < option one | option two > - anything separated by pipes (`|`) means that " \newline
                        "\u200B     you can choose any of the options in the pipe-separated list.")
   ;; \u200B is zero width space
   :usage             (str (first (:accepted-cmd-prefixes @state)) "help <? command > <? sub-command / command argument >")
   :examples          [(str (first (:accepted-cmd-prefixes @state)) "help")
                       (str (first (:accepted-cmd-prefixes @state)) "help ping")]})

;; END BOT COMMANDS

(defn- handle-message-received-event
  "Master message handler."
  [^MessageReceivedEvent event]
  (let [msg      (.. event (getMessage) (getContentDisplay))
        command? #(re-matches (:cmd-prefix @state) %)]
    (when-not (.isBot (.getAuthor event))
      (if (command? msg)
        (try
          (handle-command event)
          (catch InsufficientPermissionException ex
            (bot/send-message (.getChannel event)
              (str "**Whoops**!" \newline "It seems I don't have the permissions to do that!" \newline
                "If you think this is a mistake, try talking to a server mod or contacting `@MyriaCore#0001`.")))
          (catch IllegalArgumentException ex
            (bot/send-message (.getChannel event)
              (str "**Whoops!**" \newline "I don't recognize that command, sorry! Type `"
                (-> @state :accepted-cmd-prefixes (first)) "help` for details."))))))))


(defn- handle-state-pilot
  "Helper function for `monitor-state`. Handles the presence of the `:pilot` key. Does not modify `state`."
  [event]
  (let [msg     (bot/event->msg event)
        channel (.getChannel event)
        author  (.getAuthor event)]
    (cond
      ;; when current pilot dms bot, send DM to tracked channel
      (and (= (.getType channel) (ChannelType/PRIVATE)) (= author (-> @state :pilot :user)))
      (bot/send-message (-> @state :pilot :channel) msg)    ;; TODO: pilot sub-commands

      ;; when msg hits tracked channel, send as embed to DMs with pilot
      (and (= (-> @state :pilot :channel) channel) (not (.isBot author)))
      (let [msg-embed
            (bot/embed {:author      {:name (.getName author), :icon-url (.getAvatarUrl author)}
                        :description msg
                        :timestamp   (.. event (getMessage) (getCreationTime))})]
        (bot/send-private-message (-> @state :pilot :user) msg-embed)))))

(defn- handle-state-menus
  "Helper function for `monitor-state`. Handles the presence of the `:active-embed-menus` key."
  [event]
  (do nil))                                                 ;; TODO

(defn handle-state
  "Reads the state and performs an actions needed."
  ;; TODO: figure a better place to call this, message-recieved-handler might be too specific of an event.
  [event]
  (when (not-empty @state)
    (cond
      (instance? MessageReceivedEvent event)
      (cond
        (:pilot @state) (handle-state-pilot event)
        (:menus @state) (handle-state-menus event)))))

(defn- handle-ready-event
  "Small helper function for event-handler. Just does a few things when the bot's ready xP"
  [event]
  (let [jda (.getJDA event)]
    (println "=== Bot Ready! ===")
    (println (str "Bot is currently connected to " (count (.getGuilds jda)) " servers: " \newline
               (reduce #(str %1 \tab "- " (.getName %2) \newline) (.getGuilds jda))))))


(defn handle-event
  "Top level event handler."
  ;; TODO: Make it such that any and all JDA API related side effects happen here.
  ;; This means that, for any given event, specific event handlers should return data representing a bot's action / response.
  ;; refer to list of possible action from above.
  [event]
  (condp instance? event
    ReadyEvent (handle-ready-event event)
    MessageReceivedEvent (handle-message-received-event event)
    UserUpdateGameEvent (do nil)                            ;; was spamming during on message receive
    UserTypingEvent (do nil)                                ;; was spamming during on message receive
    MessageEmbedEvent (do nil)                              ;; was spamming on message receive
    MessageReactionAddEvent (do nil)                        ;; TODO: implement
    MessageReactionRemoveEvent (do nil)                     ;; TODO: implement
    MessageDeleteEvent (do nil)                             ;; got rly mad about tons of shit
    (do nil))                                               ;; If specific event type isnt handled, dont do anything.
  (handle-state event))

(defn -main
  "Creates and starts the bot"
  [& args]
  (bot/bot :token (first args) :event-handler (fn [%] (handle-event %))
    :presence (str "Type \"" (first (:accepted-cmd-prefixes @state)) "help\" for help!")))