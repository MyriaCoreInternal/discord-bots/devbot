(ns devbot.gdocs
  (:require [clojure.java.io]
            [clojure.java.io :as io]
            [clojure.java.data :as jdata]
            [devbot.util :refer [in?]])
  (:import com.google.api.client.auth.oauth2.Credential
           com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
           com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
           com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
           com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
           com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
           com.google.api.client.http.javanet.NetHttpTransport
           com.google.api.client.json.JsonFactory
           com.google.api.client.json.jackson2.JacksonFactory
           com.google.api.client.util.store.FileDataStoreFactory
           com.google.api.services.docs.v1.Docs
           com.google.api.services.docs.v1.DocsScopes
           com.google.api.services.docs.v1.model.Document
           (com.google.api.client.googleapis.auth.oauth2 GoogleCredential GoogleAuthorizationCodeFlow$Builder)
           (java.util Collections)
           (java.io InputStreamReader)
           (com.google.api.client.extensions.jetty.auth.oauth2 LocalServerReceiver$Builder)
           (com.google.api.services.docs.v1 Docs$Builder)
           (com.google.api.services.docs.v1.model Paragraph)))

(def ^:private application-name "Testing")
(def ^:private credentials-resource (io/resource "credentials.json"))
(def ^:private json-factory (JacksonFactory/getDefaultInstance))
(def ^:private scopes (Collections/singletonList DocsScopes/DOCUMENTS_READONLY))
(def ^:private auth-url "https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=820001492240-1g7nmgnotte3vhv82ed2qm70hoggag8a.apps.googleusercontent.com&redirect_uri=http://localhost:8888/Callback&response_type=code&scope=https://www.googleapis.com/auth/documents.readonly")

;; https://developers.google.com/docs/api/quickstart/java

(defn get-credentials
  [http-transport]
  (with-open [in (io/input-stream credentials-resource)]
    (let [client-secrets (GoogleClientSecrets/load json-factory (new InputStreamReader in))
          flow           (-> (new GoogleAuthorizationCodeFlow$Builder http-transport json-factory client-secrets scopes)
                             (.setDataStoreFactory (new FileDataStoreFactory (io/file "resources/tokens")))
                             (.setAccessType "offline")
                             (.build))
          receiver       (-> (new LocalServerReceiver$Builder) (.setPort 8888) (.build))]
      (-> (new AuthorizationCodeInstalledApp flow receiver) (.authorize "user")))))

(defn init-service
  []
  (let [http-transport (GoogleNetHttpTransport/newTrustedTransport)]
    (-> (new Docs$Builder http-transport json-factory (get-credentials http-transport))
      (.setApplicationName application-name)
      (.build))))

(defn get-document-content
  [service doc-id]
  (->> (-> service (.documents) (.get doc-id) (.execute) (.getBody) (.getContent))
    (filter #(in? "paragraph" (keys %)))
    (map #(get % "paragraph"))
    (map #(get % "elements"))
    (map (fn [x] (map #(get % "textRun") x)))
    (map (fn [x] (map #(get % "content") x)))
    (mapv #(vec %))))

(defn -main
  [& args]
  (let [service (init-service)]
    (println @auth-url)
    (clojure.pprint/pprint (get-document-content service "1tj6P8kb7JBu2_6MgOKztrtJ6RaIqv5lOfEyOC1nxIxg"))))