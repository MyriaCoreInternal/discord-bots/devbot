(ns devbot.link-parser
  (:require [hickory.core :as hickory]
            [hickory.select :as select]
            [clj-http.client :as client]
            [clojure.edn :as edn]
            [clojure.string :as str]
            [devbot.util :refer [tree-map in?]]))

(defn- get-constituent-tree
  "Helper Function for `constituent-tree`. Performs HTTPS requests to get the page the link is in,
  returns the constituent tree as a string."
  [sentence]
  (-> (client/post "https://www.link.cs.cmu.edu/cgi-bin/link/construct-page-4.cgi"
                   {:form-params  {"Constituents" "on"
                                   "InputFile"    "/scripts/input-to-parser"
                                   "LinkDisplay"  "on"
                                   "Maintainer"   "sleator@cs.cmu.edu"
                                   "NullLinks"    "on"
                                   "PageFile"     "/docs/submit-sentence-4.html"
                                   "Sentence"     (str/replace sentence #" " "+")
                                   "ShortLength"  "6"}
                    :content-type :x-www-form-urlencoded})
      :body,,, hickory/parse,,, hickory/as-hickory
      (as-> m
            (select/select (select/child (select/tag :body)
                                         (select/tag "blockquote")
                                         (select/tag :pre))
                           m))
      first
      :content
      last
      (as-> m (re-find #"(?s)Constituent tree:\s+(.*)" m))
      (last,,,)))


(defn constituent-tree
  "Takes in an english sentence `sentence` as a string and produces a syntax tree, where keywords
  are nonterminals and strings are terminals."
  [sentence]
  (let [constituents    ["WHADVP" "ADVP" "SINV" "WHNP" "NP" "PRT" "S" "VP" "ADJP" "PP" "QP" "SBAR"]]
    (tree-map
      (fn [n]
        (if-not (in? n constituents)
          (list n)
          (keyword n)))
      (tree-map str (edn/read-string (get-constituent-tree sentence))))))

(defn draw-constituent-tree
  "Draws the Constituent Tree out with pretty-printed non-terminal descriptions."
  [tree]
  (let [constituent-map {:WHADVP "",
                         :ADVP   "Adverb Phrase",
                         :SINV   "Inverted Clause",
                         :WHNP   "",
                         :NP     "Noun Phrase",
                         :PRT    "Particle",
                         :S      "Clause",
                         :VP     "Verb Phrase",
                         :ADJP   "Adjective Phrase",
                         :PP     "Prepositional Phrase",
                         :QP     "Numerical Expression",
                         :SBAR   ""}]
    (tree-map
      (fn [n]
        (if (keyword? n)
          (get constituent-map n)
          n))
      tree)))

    ;; This should get you something of this form:
    ;; (S (NP Colorless green ideas) (VP sleep (ADVP furiously)) .)
    ;; '("S" ("NP" ("Colorless green ideas")) ("VP" ("sleep") ("ADVP" ("furiously"))) (".")))