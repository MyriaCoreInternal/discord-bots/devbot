(ns devbot.discord.core
  (:require [clojure.java.data :as jdata])
  (:import [net.dv8tion.jda.core.events.message MessageReceivedEvent]
           [net.dv8tion.jda.core.entities User MessageChannel Message Message$Attachment Emote MessageReaction MessageReaction$ReactionEmote]))


(comment
  " List of Possible actions:
   - Messages
     - Send a message
       - String for msg's text content
       - Image for msg's image content
       - File for msg's file content
       - Embed for msg's embed content
     - Delete a message
     - Edit a message
       - String for msg's text content
       - Image for msg's image content
       - File for msg's file content
       - Embed for msg's embed content
   - Reactions
     - Add a reaction
     - Remove a reaction
     - Remove all reactions
   - Voice
     - (TBD)
     - (See JDA Music Bot Tutorial: https://github.com/DV8FromTheWorld/JDA/wiki/4)-Making-a-Music-Bot
     - (See LavaPlayer: https://github.com/sedmelluq/lavaplayer)
   - Users
     - Change Nickname
     - Add Roles
     - Remove Roles
   - Self (like users, but pertaining to bot's acct)
     - Change Nickname
     - Add Roles
     - Remove Roles
     - Change Icon (maybe?)
     - Add \"Playing\" message
     - Remove \"Playing\" message
     - Change \"Playing\" message
   - Guild
     - Roles
       - Add roles
       - Remove roles
       - Edit roles
     - Settings
       - Change Server Region
       - Add custom emoji
     - User Management
       - Administration
         - Kick User
         - Ban User
         - Unban User
       - Invites
         - Create Invite
         - Remove Invite)")


(defmethod jdata/from-java Emote [instance]
  (comment "todo"))                                         ;; TODO

(defmethod jdata/from-java MessageReaction [instance]
  (comment "Todo"))                                         ;;TODO

(defmethod jdata/from-java MessageReaction$ReactionEmote [instance]
  (comment "Todo"))                                         ;;TODO


(defmethod jdata/from-java User [instance]
  (cond-> {}
    (some? (-> instance (.getAsTag))) (assoc :tag (-> instance (.getAsTag)))
    (some? (-> instance (.getAvatarId))) (update :avatar #(assoc % :image-id (-> instance (.getAvatarId))))
    (some? (-> instance (.getAvatarUrl))) (update :avatar #(assoc % :image-url (-> instance (.getAvatarUrl))))
    (some? (-> instance (.getDefaultAvatarId))) (update :default-avatar #(assoc % :image-id (-> instance (.getAvatarId))))
    (some? (-> instance (.getDefaultAvatarUrl))) (update :default-avatar #(assoc % :image-url (-> instance (.getAvatarUrl))))
    (some? (-> instance (.getDiscriminator))) (assoc :discriminator (-> instance (.getDiscriminator)))
    (some? (-> instance (.getEffectiveAvatarUrl))) (assoc :effective-avatar-url (-> instance (.getEffectiveAvatarUrl)))
    (some? (-> instance (.getJDA))) (assoc :jda (-> instance (.getJDA)))
    (some? (-> instance (.getMutualGuilds))) (assoc :mutual-guilds (-> instance (.getMutualGuilds)))
    (some? (-> instance (.getName))) (assoc :name (-> instance (.getName)))
    (some? (-> instance (.hasPrivateChannel))) (assoc :private-channel-open? (-> instance (.hasPrivateChannel)))
    (some? (-> instance (.isBot))) (assoc :bot? (-> instance (.isBot)))
    (some? (-> instance (.isFake))) (-> instance (.isFake))
    (some? (-> instance (.getAsMention))) (assoc :mention (-> instance (.getAsMention)))
    (some? (-> instance (.getId))) (assoc :id (-> instance (.getId)))
    (some? (-> instance (.getCreationTime))) (assoc :creation-time (-> instance (.getCreationTime)))))

(defn user->private-channel
  "Returns a user's private channel"
  [user]
  (-> (:java-object user) (.openPrivateChannel) (.complete)))

(defmethod jdata/from-java MessageReceivedEvent [instance]
  (cond-> {}
    (some? (-> instance (.getAuthor))) (assoc :author (-> instance (.getAuthor)))
    (some? (-> instance (.getMessage))) (assoc :message (-> instance (.getMessage)))
    (some? (-> instance (.getMember))) (assoc :member (-> instance (.getMember)))
    (some? (-> instance (.isWebhookMessage))) (assoc :webhook-message? (-> instance (.isWebhookMessage)))
    (some? (-> instance (.getJDA))) (assoc :jda (-> instance (.getJDA)))
    (some? (-> instance (.getChannel))) (assoc :channel (-> instance (.getChannel)))
    (some? (-> instance (.getGuild))) (assoc :guild (-> instance (.getGuild)))
    (some? (-> instance (.getMessageId))) (assoc :message-id (-> instance (.getMessageId)))
    (some? (-> instance (.getGroup))) (assoc :group (-> instance (.getGroup)))
    (some? (-> instance (.getChannelType))) (assoc :channel-type (-> instance (.getChannelType)))
    (some? (-> instance (.getPrivateChannel))) (assoc :private-channel (-> instance (.getPrivateChannel)))
    (some? (-> instance (.getTextChannel))) (assoc :text-channel (-> instance (.getTextChannel)))
    (some? (-> instance (.getResponseNumber))) (assoc :response-number (-> instance (.getResponseNumber)))))

