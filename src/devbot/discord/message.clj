(ns devbot.discord.message
  (:require [clojure.java.data :as jdata])
  (:import [net.dv8tion.jda.core.events.message MessageReceivedEvent]
           [net.dv8tion.jda.core.entities User MessageHistory MessageChannel Message Message$Attachment Emote MessageReaction MessageReaction$ReactionEmote]
           (clojure.lang APersistentMap)
           (net.dv8tion.jda.core MessageBuilder)))

;; NOTE: should probably be a lazyseq.
(defmethod jdata/from-java MessageHistory [instance]
  (comment "TODO")) ;; TODO

(defn get-history-after
  [message-history message-channel]
  (comment "TODO")) ;; TODO

(defn get-history-after
  [message-history message-channel]
  (comment "TODO")) ;; TODO

(defn get-history-after
  [message-history message-channel]
  (comment "TODO")) ;; TODO

(defmethod jdata/from-java Message$Attachment [instance]
  (cond-> {}
    (some? (-> instance (.getFileName))) (assoc :file-name (-> instance (.getFileName)))
    (some? (-> instance (.getJDA))) (assoc :jda (-> instance (.getJDA)))
    (some? (-> instance (.getProxyUrl))) (assoc :proxy-url (-> instance (.getProxyUrl)))
    (some? (-> instance (.isImage))) (assoc :image? (-> instance (.isImage)))
    (some? (-> instance (.getHeight))) (update-in [:image :dimensions] #(assoc % :height (-> instance (.getHeight))))
    (some? (-> instance (.getWidth))) (update-in [:image :dimensions] #(assoc % :width (-> instance (.getWidth))))
    (some? (-> instance (.getAsIcon))) (update :image #(assoc % :image->icon (-> instance (.getAsIcon))))
    (some? (-> instance (.getSize))) (assoc :file-size (-> instance (.getSize)))
    (some? (-> instance (.getUrl))) (assoc :url (-> instance (.getUrl)))
    (some? (-> instance (.getId))) (assoc :id (-> instance (.getId)))
    (some? (-> instance (.getCreationTime))) (assoc :creation-time (-> instance (.getCreationTime)))))

(defmethod jdata/from-java Message [instance]
  (cond-> {}
    (some? (-> instance (.getContentDisplay))) (update :content #(assoc % :display (-> instance (.getContentDisplay))))
    (some? (-> instance (.getContentRaw))) (update :content #(assoc % :raw (-> instance (.getContentRaw))))
    (some? (-> instance (.getContentStripped))) (update :content #(assoc % :stripped (-> instance (.getContentStripped))))
    (some? (-> instance (.getAttachments))) (assoc :attachments (-> instance (.getAttachments)))
    (some? (-> instance (.getAuthor))) (assoc :author (-> instance (.getAuthor)))
    (some? (-> instance (.getCategory))) (assoc :category (-> instance (.getCategory)))
    (some? (-> instance (.getChannel))) (assoc :channel (-> instance (.getChannel)))
    (some? (-> instance (.getEmotes))) (assoc :emotes (-> instance (.getEmotes)))
    (some? (-> instance (.getGuild))) (assoc :guild (-> instance (.getGuild)))
    (some? (-> instance (.getGroup))) (assoc :group (-> instance (.getGroup)))
    (some? (-> instance (.getJumpUrl))) (assoc :jump-url (-> instance (.getJumpUrl)))
    (some? (-> instance (.getEmbeds))) (assoc :embeds (-> instance (.getEmbeds)))
    (some? (-> instance (.getInvites))) (assoc :invites (-> instance (.getInvites)))
    (some? (-> instance (.getJDA))) (assoc :jda (-> instance (.getJDA)))
    (some? (-> instance (.getMember))) (assoc :member (-> instance (.getMember)))
    (some? (-> instance (.getMember))) (assoc :member (-> instance (.getMember)))
    (some? (-> instance (.getMentionedChannels))) (update :mentioned #(assoc % :everything (-> instance (.getMentions))))
    (some? (-> instance (.getMentionedChannels))) (update :mentioned #(assoc % :channels (-> instance (.getMentionedChannels))))
    (some? (-> instance (.getMentionedMembers))) (update :mentioned #(assoc % :members (-> instance (.getMentionedMembers))))
    (some? (-> instance (.getMentionedUsers))) (update :mentioned #(assoc % :users (-> instance (.getMentionedUsers))))
    (some? (-> instance (.getMentionedRoles))) (update :mentioned #(assoc % :roles (-> instance (.getMentionedRoles))))
    (some? (-> instance (.mentionsEveryone))) (update :mentioned #(assoc % :everyone? (-> instance (.mentionsEveryone))))
    (some? (-> instance (.getReactions))) (assoc :reactions (-> instance (.getReactions)))
    (some? (-> instance (.isPinned))) (assoc :pinned? (-> instance (.isPinned)))
    (some? (-> instance (.isTTS))) (assoc :tts? (-> instance (.isTTS)))
    (some? (-> instance (.isEdited))) (assoc :edited? (-> instance (.isEdited)))
    (some? (-> instance (.getEditedTime))) (assoc :edit-time (-> instance (.getEditedTime)))
    (some? (-> instance (.getType))) (assoc :channel-type (-> instance (.getType)))))

(defmethod jdata/to-java [Message APersistentMap] [clazz props]
  (cond-> (new MessageBuilder)
    (some? (:content (:raw props))) (.setContent (:content (:raw props)))
    (some? (:tts? props)) (.setTTS (:tts? props))
    (some? (:tts? props)) (.setTTS (:tts? props))
    (some? (:embeds props)) (.setEmbed (first (:embeds props)))
    :finally (.build)))