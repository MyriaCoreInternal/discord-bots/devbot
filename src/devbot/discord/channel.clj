(ns devbot.discord.channel
  (:require [clojure.java.data :as jdata])
  (:import [net.dv8tion.jda.core.events.message MessageReceivedEvent]
           [net.dv8tion.jda.core.entities User MessageChannel Message Message$Attachment Emote MessageReaction MessageReaction$ReactionEmote]))

(defmethod jdata/from-java MessageChannel [instance]
  (cond-> {}
    (some? (-> instance (.getJDA))) (assoc :jda (-> instance (.getJDA)))
    (some? (-> instance (.getCreationTime))) (assoc :creation-time (-> instance (.getCreationTime)))
    (some? (-> instance (.getId))) (assoc :id (-> instance (.getId)))
    (some? (-> instance (.getName))) (assoc :name (-> instance (.getName)))
    (some? (-> instance (.getHistory))) (assoc :history (-> instance (.getHistory)))
    (some? (-> instance (.getLatestMessageId))) (assoc :latest-message-id (-> instance (.getLatestMessageId)))
    (some? (-> instance (.getChannelType))) (assoc :channel-type (-> instance (.getChannelType)))
    (some? (-> instance (.getPinnedMessages) (.queue))) (assoc :pinned-messages (-> instance (.getPinnedMessages) (.queue))))) ;; Todo: make sure this doesnt fuck shit up

