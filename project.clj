(defproject devbot "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/core.match "0.3.0-alpha5"]
                 ;; https://mvnrepository.com/artifact/net.dv8tion/JDA
                 [net.dv8tion/JDA "3.8.1_448"]
                 [instaparse "1.4.9"]
                 [rhizome "0.2.9"]
                 [clj-http "3.9.1"]
                 [hickory "0.7.1"]
                 [clojure.java-time "0.3.2"]
                 [com.google.api-client/google-api-client "1.28.0"]
                 [com.google.oauth-client/google-oauth-client-jetty "1.23.0"]
                 [com.google.apis/google-api-services-docs "v1-rev20190128-1.28.0"]
                 [org.clojure/java.data "0.1.1"]]
  :repositories [["jcenter" "https://jcenter.bintray.com"]]
  :profiles {:uberjar {:aot :all}}
  :source-paths ["src"]
  :aot [devbot.core]
  :main devbot.core
  :jar-name "devbot"
  :uberjar-name "devbot-standalone.jar")
